"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const queue = require("queue");
const Marks_1 = require("../models/Marks");
const Countries_1 = require("../models/Countries");
const Collors_1 = require("../models/Collors");
const Galleries_1 = require("../models/Galleries");
var RelationHelper;
(function (RelationHelper) {
    /**
     * Подгружает связи к продукции, такие как производитель, цвета, марка
     * @param models
     * @param category
     * @returns {Promise|Promise<T>}
     */
    function loadRelations(models, category) {
        let marksList = [], collorsList = [], countryList = [], galleryList = [];
        return new Promise((succeed, fail) => {
            let q = queue();
            // country
            q.push(function (next) {
                Countries_1.default.find(function (error, data) {
                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            countryList[data[i].id] = data[i].name;
                        }
                    }
                    next();
                });
            });
            // color
            q.push(function (next) {
                Collors_1.default.find(function (error, data) {
                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            collorsList[data[i].id] = data[i].name;
                        }
                    }
                    next();
                });
            });
            // mark
            q.push(function (next) {
                Marks_1.default.find(function (error, data) {
                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            marksList[data[i].id] = data[i].name;
                        }
                    }
                    next();
                });
            });
            // gallery
            q.push(function (next) {
                Galleries_1.default.find({ category: category }, function (error, data) {
                    if (!error && data.length > 0) {
                        for (let i = 0; i < data.length; i++) {
                            //let id = data[i].item;
                            //if (!galleryList[id]) galleryList[id] = [];
                            //galleryList[id].push(data[i]);
                        }
                    }
                    next();
                });
            });
            q.start(function (err) {
                let id = "";
                console.log('relationsHelers', models.length);
                for (let i = 0; i < models.length; i++) {
                    id = models[i]["mark"];
                    console.log('mark', id);
                    if (id) {
                        if (marksList[id])
                            models[i]["mark"] = marksList[id];
                        else
                            models[i]["mark"] = "";
                    }
                    id = models[i]["color"];
                    if (id) {
                        if (collorsList[id])
                            models[i]["color"] = collorsList[id];
                        else
                            models[i]["color"] = "";
                    }
                    id = models[i]["manufacturer"];
                    if (id) {
                        if (countryList[id])
                            models[i]["manufacturer"] = countryList[id];
                        else
                            models[i]["manufacturer"] = "";
                    }
                    if (category) {
                        models[i].category = category;
                    }
                    if (galleryList[models[i].id] && galleryList[models[i].id].length > 0) {
                        models[i].gallery = galleryList[models[i].id];
                    }
                    //console.log('models category', models[i], models[i]['category']);
                }
                if (err) {
                    if (fail && typeof (fail) == "function")
                        fail(err);
                }
                else if (succeed && typeof (succeed) == "function")
                    succeed(models);
            });
        });
    }
    RelationHelper.loadRelations = loadRelations;
    /**
     * Опредеяем класс продукции по категории
     * @returns {Array<IClass>}
     */
    function getProductClasses() {
        let list = [];
        list.push({ name: "Холодильники", table: "Productsfridges", slug: "fridges" });
        list.push({ name: "Стиральные машинки", table: "Productswashings", slug: "washings" });
        list.push({ name: "Панели", table: "Productshobs", slug: "hobs" });
        list.push({ name: "Кондиционеры", table: "Productsconditioners", slug: "conditioners" });
        list.push({ name: "Духовые шкафы", table: "Productsovens", slug: "ovens" });
        return list;
    }
    RelationHelper.getProductClasses = getProductClasses;
    /**
     * Выводит таблицу по названию категории
     * @param category
     * @returns {any}
     */
    function getProductClass(category) {
        let list = getProductClasses();
        if (category) {
            return list.find((element) => {
                if (element.slug == category)
                    return element;
            });
        }
        return "";
    }
    RelationHelper.getProductClass = getProductClass;
})(RelationHelper = exports.RelationHelper || (exports.RelationHelper = {}));
