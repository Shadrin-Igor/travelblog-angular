import * as mongoose from "mongoose";

export default mongoose.model("Productsconditioners",
    new mongoose.Schema({
        id: {
            type: String,
            required: true
        },
        gallery: [],
        mark: {
            type: String,
            required: true
        },
        model: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true
        },
        category: {
            type: String,
        },
        image: {
            type: String,
        },
        image_dir: {
            type: String,
        },
        price: {
            type: Number,
        },
        // Цвет вн. Блока
        color: {
            type: String,
        },
        // Производитель
        manufacturer: {
            type: String,
            required: true
        },
        // Тип
        type: {
            type: String,
        },
        // Обслуживаемая площадь
        serviced_area: {
            type: String
        },
        // Класс энергопотребления
        energy_class: {
            type: String
        },
        // Основные режимы
        modes: {
            type: String
        },
        // Мощность в режиме охлаждения
        cooling_capacity: {
            type: String
        },
        // Мощность в режиме обогрева
        heating_power: {
            type: String
        },
        // Потребляемая мощность при обогреве
        power_heating: {
            type: String
        },
        // Потребляемая мощность при охлаждении
        power_cooling: {
            type: String
        },
        // Дополнительные режимы
        additional_modes: {
            type: String
        },
        // Режим осушения
        drying_mode: {
            type: String
        },
        // д/y
        d_u: {
            type: String
        },
        // Таймер включения/выключения
        timer: {
            type: String
        },
        // Уровень шума внутреннего блока
        noise_level: {
            type: String
        },
        // Фаза
        phase: {
            type: String
        },
        // Фильтры тонкой очистки воздуха
        air_filters: {
            type: String
        },
        // Регулировка скорости вращения вентилятора
        fan_control: {
            type: String
        },
        // Минимальная температура для эксплуатации кондиционера в режиме обогрева
        minimum_temperature: {
            type: String
        },
        // Дополнительная информация
        additional_information: {
            type: String
        },
        // Другие функции и особенности
        other_functions: {
            type: String
        },
        // Габариты внутреннего блока
        dimensions: {
            type: String
        },
        // Габариты наружнего блока
        weight_indoor: {
            type: String
        },
        // Вес внутреннего блока
        dimensions_outdoor: {
            type: String
        },
        // Вес внешнего блока
        weight_external: {
            type: String
        },
        // фреон
        freon: {
            type: String
        }
    })
);