import * as mongoose from "mongoose";

export default mongoose.model("Productshobs",
    new mongoose.Schema({
        id: {
            type: String,
            required: true
        },
        gallery: [],
        mark: {
            type: String,
            required: true
        },
        model: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true
        },
        category: {
            type: String,
        },
        image: {
            type: String,
        },
        image_dir: {
            type: String,
        },
        // Производитель
        manufacturer: {
            type: String,
            required: true
        },
        price: {
            type: Number,
        },
        // Тип панели
        type: {
            type: String,
        },
        // Габариты 
        dimensions: {
            type: String
        },
        // Размеры для встраивания
        dimensions_embedding: {
            type: String
        },
        // Материал панели 
        material: {
            type: String
        },
        // Всего конфорок
        total_hotplates: {
            type: String
        },
        // Газовых конфорок
        gas_burners: {
            type: String
        },
        // Тип упровления
        type_consolidation: {
            type: String
        },
        // Расположение панели упр.
        location_control: {
            type: String
        },
        // Система безопастности
        security_system: {
            type: String
        },
        // Электроподжиг
        electric_ignition: {
            type: String
        },
        // Газ-контроль конфорок
        gas_control: {
            type: String
        },
        // Чугунные решетки
        iron_grilles: {
            type: String
        },
        // Цвет
        color: {
            type: String
        },
        // Тип дисплея
        display_type: {
            type: String
        }
    })
);