"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("Productswashings", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    image: {
        type: String,
    },
    slug: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    model: {
        type: String,
        required: true
    },
    mark: {
        type: String,
    },
    manufacturer: {
        type: String,
        required: true
    },
    price: {
        type: Number
    },
    // Тип загрузки
    download_type: {
        type: String
    },
    // Максимальная загрузка
    maximum_load: {
        type: String
    },
    // Сушка
    drying: {
        type: String
    },
    // Управление
    control: {
        type: String
    },
    // Дисплей
    display: {
        type: String
    },
    // Прямой привод
    direct_drive: {
        type: String
    },
    // Габариты
    dimensions: {
        type: String
    },
    // Вес
    weight: {
        type: String
    },
    //Цвет
    color: {
        type: String
    },
    // Энергоэффективность(Класс энергопотребления)
    energy_class: {
        type: String
    },
    // Количество оборотов
    number_revolutions: {
        type: String
    },
    // Защита от детей
    protection_children: {
        type: String
    },
    // Контроль дисбаланса
    control_imbalance: {
        type: String
    },
    // Контроль за уровнем пены
    foam_control: {
        type: String
    },
    // Количество программ
    number_programs: {
        type: String
    },
    // Специальные программы
    programs: {
        type: String
    },
    // Таймер отсрочки начала стирки
    delay_timer: {
        type: String
    },
    // Материал бака
    material: {
        type: String
    },
    // Загрузочный люк
    loading_door: {
        type: String
    },
    // Уровень шума
    noise: {
        type: String
    },
    // Догрузка белья
    loading_linen: {
        type: String
    },
    // Дополнительные возможности
    additional_features: {
        type: String
    },
    // Годовой расход воды
    annual_consumption: {
        type: String
    },
    // Опция на максимальную температуру
    maximum_temperature: {
        type: String
    },
    // Дверца
    door: {
        type: String
    }
}));
