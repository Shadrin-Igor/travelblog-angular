import * as mongoose from "mongoose";

export default mongoose.model("Productsovens",
    new mongoose.Schema({
        id: {
            type: String,
            required: true
        },
        gallery: [],
        mark: {
            type: String,
            required: true
        },
        model: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true
        },
        category: {
            type: String,
        },
        image: {
            type: String,
        },
        image_dir: {
            type: String,
        },
        // Производитель
        manufacturer: {
            type: String,
            required: true
        },
        price: {
            type: Number,
        },

        // Тип духовки
        type: {
            type: String,
        },
        // Упровление
        controls: {
            type: String
        },
        // Объём
        scope: {
            type: String
        },
        // Размеры 
        dimensions: {
            type: String
        },
        // Режимов работы
        modes_operation: {
            type: String
        },
        // Гриль
        grill: {
            type: String
        },
        // Конвекция
        convection: {
            type: String
        },
        // Размер ниши
        niche_size: {
            type: String
        },
        // Вертел
        rotisserie: {
            type: String
        },
        // Дверца духовки
        oven_door: {
            type: String
        },
        // Освещение
        lighting: {
            type: String
        },
        // Комплектация
        equipment: {
            type: String
        },
        // Класс энергоэфективности
        energy_class: {
            type: String
        },
        // Максимальная температура
        maximum_temperature: {
            type: String
        },
        // Таймер выключения
        sleep_timer: {
            type: String
        },
        // индикация
        indication: {
            type: String
        },
        // цвет
        color: {
            type: String
        },
        // Тип дисплея
        display_type: {
            type: String
        }
    })
);