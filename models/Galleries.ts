import * as mongoose from 'mongoose';

// User
let Galleries = new mongoose.Schema({
    dir: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    item: {
        type: String,
        required: true
    },
});

export default mongoose.model('Galleries', Galleries);