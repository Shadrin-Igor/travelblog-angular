var mongoose = require('mongoose');

module.exports = mongoose.model('posts_categories',
    new mongoose.Schema({
        id: { type: Number, required: true, unique: true },
        name: { type: String, required: true }
    })
);