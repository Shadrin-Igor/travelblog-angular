"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const relationHelper_1 = require("../libs/relationHelper");
let express = require("express"), router = express.Router(), queue = require("queue"), errorQueue = "";
const Marks_1 = require("../models/Marks");
//import * as relationHelper from '../libs/relationHelper';
/**
 * Базовые метод для просмотра продукции
 */
router.get("/(:brand).html", (req, res, next) => {
    if (req.params.brand) {
        let modelClasses = relationHelper_1.RelationHelper.getProductClasses(), q = queue({ concurrency: 1 }), allItems = {}, errorQueue = "", listBrands = [], markModel = {}, model;
        q.push((next) => {
            Marks_1.default.findOne({ slug: req.params.brand }, (error, mark) => {
                if (!error && mark) {
                    markModel = mark;
                }
                next();
            });
        });
        if (markModel) {
            for (let i = 0; i < modelClasses.length; i++) {
                // Описание товара
                q.push(function (next) {
                    model = require("../models/" + modelClasses[i].table);
                    model.default.find({ mark: markModel.id })
                        .then(function (list, err) {
                        if (!err) {
                            if (list && list.length > 0) {
                                relationHelper_1.RelationHelper.loadRelations(list, modelClasses[i])
                                    .then(function (result) {
                                    //console.log('add', modelClasses[i].table, result );
                                    allItems[modelClasses[i].slug] = result;
                                    next();
                                }, function (error) {
                                    console.log("error", error, modelClasses[i].table);
                                    errorQueue = error;
                                    next();
                                });
                            }
                            else {
                                next();
                            }
                        }
                        else {
                            errorQueue = err;
                            console.log("error Queue", modelClasses[i].table, err);
                            next();
                        }
                    });
                });
            }
        }
        // Собираем список брендов
        q.push((next) => {
            Marks_1.default.find({}).sort("name").exec((err, data) => {
                if (err) {
                    next(err);
                }
                else {
                    listBrands = data;
                    next();
                }
            });
        });
        q.start((error) => {
            if (!errorQueue) {
                //console.log('allItems', allItems);
                res.render("brand-list", { allItems: allItems, listBrands: listBrands, RelationHelper: relationHelper_1.RelationHelper });
            }
            else {
                //res.send(errorQueue)
                console.log("errorQueue", errorQueue);
                res.redirect("/404.html");
            }
        });
    }
    else
        res.redirect("/404.html");
});
module.exports = router;
